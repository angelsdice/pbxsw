import React from 'react';
import Loadable from 'react-loadable';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Loader from '../components/SharedComponents/Loader';
import ErrorBoundary from '../components/SharedComponents/ErrorBoundary';

const LoadableProductsPage = Loadable({
  loader: () =>
    import(/* webpackChunkName: "Products" */ '../containers/ProductsContainer'),
  loading: Loader
});

const Routes = () => (
  <ErrorBoundary>
    <Router>
      <Switch>
        <Route exact path="/" component={LoadableProductsPage} />
        <Route component={NoMatch} />
      </Switch>
    </Router>
  </ErrorBoundary>
);

function NoMatch({ location }) {
  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

export default Routes;
