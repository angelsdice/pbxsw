export const fetchProducts = () => {
  return fetch(`/api/products`)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response;
    })
    .then(response => response.json());
};
