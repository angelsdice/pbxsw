import reducer from '../ProductsReducer';
import * as types from '../../actions/actionTypes';

const initialState = {
  productsLoading: true,
  error: false,
  products: []
};

describe('switches reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle single LOADING_PRODUCTS', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: types.LOADING_PRODUCTS
        }
      )
    ).toEqual({
      productsLoading: true,
      error: false,
      products: []
    });
  });

  it('should handle single LOADED_PRODUCTS', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: types.LOADED_PRODUCTS,
          payload: { products: [{ name: 'product1' }] }
        }
      )
    ).toEqual({
      productsLoading: false,
      error: false,
      products: [{ name: 'product1' }]
    });
  });
  it('should handle single ERROR_PRODUCTS', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: types.ERROR_PRODUCTS
        }
      )
    ).toEqual({
      productsLoading: false,
      error: true,
      products: []
    });
  });
});
