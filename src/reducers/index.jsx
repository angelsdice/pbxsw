import { combineReducers } from 'redux';
import products from './ProductsReducer';
const rootReducer = combineReducers({
  products
});

export default rootReducer;
