import * as types from '../actions/actionTypes';

const initialState = {
  productsLoading: true,
  error: false,
  products: []
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOADING_PRODUCTS:
      return {
        ...state,
        productsLoading: true
      };
    case types.LOADED_PRODUCTS:
      return {
        ...state,
        productsLoading: false,
        products: action.payload.products
      };
    case types.ERROR_PRODUCTS:
      return {
        ...state,
        error: true,
        productsLoading: false
      };
    default:
      return state;
  }
};

export default productsReducer;
