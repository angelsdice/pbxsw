import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import ErrorBoundary from '.';

class ErrorComponent extends Component {
  componentWillMount() {
    this.setState({
      hasError: true, // eslint-disable-line
      error: 'error name', // eslint-disable-line
      errorInfo: 'error info' // eslint-disable-line
    });
  }
}

storiesOf('SharedComponents/ErrorBoundary', module).add(
  'Default',
  withInfo()(() => (
    <ErrorBoundary>
      <ErrorComponent />
    </ErrorBoundary>
  ))
);
