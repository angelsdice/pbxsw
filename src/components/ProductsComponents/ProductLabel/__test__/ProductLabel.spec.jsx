import React from 'react';
import ProductLabel from '..';

function setup() {
  const enzymeWrapper = global.mount(
    <ProductLabel productLabel="bestseller" />
  );
  return {
    enzymeWrapper
  };
}
describe('components', () => {
  describe('ProductLabel', () => {
    it('should render self', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find(ProductLabel).length).toBe(1);
      expect(enzymeWrapper.find('div').length).toBe(1);
      expect(enzymeWrapper.find('div').text()).toEqual('bestseller');
    });
  });
});
