import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import ProductLabel from '.';

storiesOf('ProductComponents/ProductLabel', module)
  .addDecorator(story => (
    <div style={{ height: '100px', position: 'relative' }}>{story()}</div>
  ))
  .add('Default', withInfo()(() => <ProductLabel productLabel="bestseller" />));
