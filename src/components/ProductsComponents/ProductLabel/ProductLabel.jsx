import React from 'react';
import styles from './ProductLabel.module.scss';

const ProductLabel = ({ productLabel }) => (
  <div className={styles.productLabel}>{productLabel}</div>
);

export default ProductLabel;
