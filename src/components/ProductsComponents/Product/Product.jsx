import React from 'react';
import styles from './Product.module.scss';
import ProductHeader from '../ProductHeader';
import ProductPrice from '../ProductPrice';
import ProductButton from '../ProductButton';

const Product = ({ product }) => (
  <article className="col-lg-4 col-md-6 col-sm-12">
    <div className={styles.productBox}>
      <ProductHeader
        productImage={product.image}
        productLabel={product.productLabel}
      />
      <div className={styles.productContent}>
        <h2 className={styles.productTitle}>{product.title}</h2>
        <p className={styles.productDescription}>{product.description}</p>
        <div>
          <ProductPrice
            priceLabel={product.priceLabel}
            currency={product.currency}
            price={product.price}
          />
          <ProductButton ctaLink={product.ctaLink} ctaText={product.cta} />
        </div>
      </div>
    </div>
  </article>
);

export default Product;
