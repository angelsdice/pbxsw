import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import Product from '.';

const product = {
  title: 'Simple Canvas',
  description: 'Lets your pictures speak for themselves.',
  image: {
    path: 'images/product.jpg',
    alt: 'Simple Canvas'
  },
  price: 1500,
  currency: '£',
  priceLabel: 'From',
  productLabel: 'bestseller',
  cta: 'Shop Now',
  ctaLink: '/random/link/to/no/where'
};

storiesOf('ProductComponents/Product', module).add(
  'Default',
  withInfo()(() => <Product product={product} />)
);
