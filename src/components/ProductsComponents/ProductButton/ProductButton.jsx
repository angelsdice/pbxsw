import React from 'react';
import styles from './ProductButton.module.scss';

const ProductButton = ({ ctaLink, ctaText }) => (
  <a href={ctaLink} className={styles.productCtaLink}>
    {ctaText}
  </a>
);

export default ProductButton;
