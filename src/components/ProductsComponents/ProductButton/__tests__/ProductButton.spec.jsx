import React from 'react';
import ProductButton from '..';

function setup() {
  const enzymeWrapper = global.mount(
    <ProductButton ctaLink="https://photobox.com" ctaText="Buy" />
  );
  return {
    enzymeWrapper
  };
}
describe('components', () => {
  describe('ProductButton', () => {
    it('should render self', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find(ProductButton).length).toBe(1);
      expect(enzymeWrapper.find('a').length).toBe(1);
      expect(enzymeWrapper.find('a').props().href).toEqual(
        'https://photobox.com'
      );
      expect(enzymeWrapper.find('a').props().children).toEqual('Buy');
    });
  });
});
