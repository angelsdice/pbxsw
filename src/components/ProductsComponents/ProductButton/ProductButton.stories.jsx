import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import ProductButton from '.';

storiesOf('ProductComponents/ProductButton', module).add(
  'Default',
  withInfo()(() => (
    <ProductButton ctaLink="https://photobox.com" ctaText="Buy" />
  ))
);
