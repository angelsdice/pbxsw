import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import ProductPrice from '.';

storiesOf('ProductComponents/ProductPrice', module).add(
  'Default',
  withInfo()(() => <ProductPrice priceLabel="From" currency="£" price="1000" />)
);
