import React from 'react';
import ProductPrice from '..';

function setup() {
  const enzymeWrapper = global.mount(
    <ProductPrice priceLabel="From" currency="£" price="1000" />
  );
  return {
    enzymeWrapper
  };
}
describe('components', () => {
  describe('ProductPrice', () => {
    it('should render self', () => {
      const enzymeWrapper = global.mount(
        <ProductPrice priceLabel="From" currency="£" price="1000" />
      );
      expect(enzymeWrapper.find(ProductPrice).length).toBe(1);
      expect(enzymeWrapper.find('p').length).toBe(1);
      expect(enzymeWrapper.find('span').length).toBe(1);
      expect(enzymeWrapper.find('span').text()).toEqual('From');
      expect(enzymeWrapper.find('p').text()).toEqual('From£10.00');
    });
    it('should handle different currency', () => {
      const enzymeWrapper = global.mount(
        <ProductPrice priceLabel="From" currency="$" price="1000" />
      );
      expect(enzymeWrapper.find(ProductPrice).length).toBe(1);
      expect(enzymeWrapper.find('p').length).toBe(1);
      expect(enzymeWrapper.find('span').length).toBe(1);
      expect(enzymeWrapper.find('span').text()).toEqual('From');
      expect(enzymeWrapper.find('p').text()).toEqual('From$10.00');
    });
    it('should handle no priceLabel', () => {
      const enzymeWrapper = global.mount(
        <ProductPrice priceLabel="" currency="$" price="1000" />
      );
      expect(enzymeWrapper.find(ProductPrice).length).toBe(1);
      expect(enzymeWrapper.find('p').length).toBe(1);
      expect(enzymeWrapper.find('span').length).toBe(0);
      expect(enzymeWrapper.find('p').text()).toEqual('$10.00');
    });
  });
});
