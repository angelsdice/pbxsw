import React from 'react';
import styles from './ProductPrice.module.scss';

const ProductPrice = ({ priceLabel, currency, price }) => (
  <p className={styles.productPrice}>
    {priceLabel && (
      <span className={styles.productPriceLabel}>{priceLabel}</span>
    )}
    {currency}
    {Number(price / 100).toFixed(2)}
  </p>
);

export default ProductPrice;
