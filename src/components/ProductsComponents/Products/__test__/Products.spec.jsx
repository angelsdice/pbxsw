import React from 'react';
import Products from '..';

const products = [
  {
    title: 'Simple Canvas1',
    description: 'Lets your pictures speak for themselves.',
    image: {
      path: 'images/product.jpg',
      alt: 'Simple Canvas'
    },
    price: 1500,
    currency: '£',
    priceLabel: 'From',
    productLabel: 'bestseller',
    cta: 'Shop Now',
    ctaLink: '/random/link/to/no/where'
  },
  {
    title: 'Simple Canvas2',
    description: 'Lets your pictures speak for themselves.',
    image: {
      path: 'images/product.jpg',
      alt: 'Simple Canvas'
    },
    price: 1500,
    currency: '£',
    priceLabel: 'From',
    productLabel: 'bestseller',
    cta: 'Shop Now',
    ctaLink: '/random/link/to/no/where'
  },
  {
    title: 'Simple Canvas3',
    description: 'Lets your pictures speak for themselves.',
    image: {
      path: 'images/product.jpg',
      alt: 'Simple Canvas'
    },
    price: 1500,
    currency: '£',
    priceLabel: 'From',
    productLabel: 'bestseller',
    cta: 'Shop Now',
    ctaLink: '/random/link/to/no/where'
  }
];

describe('components', () => {
  describe('Products', () => {
    it('should render correct number of products', () => {
      const enzymeWrapper = global.mount(<Products products={products} />);
      expect(enzymeWrapper.find(Products).length).toBe(1);
      expect(enzymeWrapper.find('div.container').length).toBe(1);
      expect(enzymeWrapper.find('div.row').length).toBe(1);
      expect(enzymeWrapper.find('Product').length).toBe(3);
    });
  });
});
