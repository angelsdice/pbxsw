import React from 'react';
import Product from '../Product';

const Products = ({ products }) => {
  return (
    <div className="container">
      <div className="row">
        {products.map(product => {
          return <Product product={product} key={product.title} />;
        })}
      </div>
    </div>
  );
};

export default Products;
