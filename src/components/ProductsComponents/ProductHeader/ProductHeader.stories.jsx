import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import ProductHeader from '.';

const productImage = {
  path: 'images/product.jpg',
  alt: 'Simple Canvas'
};
const productLabel = 'bestseller';

storiesOf('ProductComponents/ProductHeader', module).add(
  'Default',
  withInfo()(() => (
    <ProductHeader productImage={productImage} productLabel={productLabel} />
  ))
);
