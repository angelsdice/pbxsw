import React from 'react';
import ProductLabel from '../ProductLabel';
import styles from './ProductHeader.module.scss';

const ProductHeader = ({ productImage, productLabel }) => (
  <header className={styles.productHeader}>
    {productImage && (
      <img
        className={styles.productImage}
        src={productImage.path}
        alt={productImage.alt}
      />
    )}
    {productLabel && (
      <>
        <ProductLabel productLabel={productLabel} />
      </>
    )}
  </header>
);

export default ProductHeader;
