import React from 'react';
import ProductHeader from '..';

describe('components', () => {
  describe('ProductHeader', () => {
    it('should render self', () => {
      const productImage = {
        path: 'images/product.jpg',
        alt: 'Simple Canvas'
      };
      const productLabel = 'bestseller';
      const enzymeWrapper = global.mount(
        <ProductHeader
          productImage={productImage}
          productLabel={productLabel}
        />
      );
      expect(enzymeWrapper.find(ProductHeader).length).toBe(1);
      expect(enzymeWrapper.find('img').length).toBe(1);
      expect(enzymeWrapper.find('ProductLabel').length).toBe(1);
      expect(enzymeWrapper.find('ProductLabel').text()).toEqual('bestseller');
    });
    it('should not render label if empty', () => {
      const productImage = {
        path: 'images/product.jpg',
        alt: 'Simple Canvas'
      };
      const productLabel = '';
      const enzymeWrapper = global.mount(
        <ProductHeader
          productImage={productImage}
          productLabel={productLabel}
        />
      );
      expect(enzymeWrapper.find(ProductHeader).length).toBe(1);
      expect(enzymeWrapper.find('img').length).toBe(1);
      expect(enzymeWrapper.find('ProductLabel').length).toBe(0);
    });
  });
});
