import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import Loader from '../../components/SharedComponents/Loader';
import Products from '../../components/ProductsComponents/Products';
import * as ProductsActions from '../../actions/ProductsActions';

export class ProductsContainer extends React.Component {
  componentDidMount() {
    const { ProductsActions } = this.props;
    ProductsActions.loadProducts();
  }
  render() {
    const { productsLoading, products, error } = this.props;
    if (error) {
      return <Redirect push to="/ErrorPage" />;
    }
    if (productsLoading) {
      return <Loader />;
    }
    return <Products products={products} />;
  }
}

export const mapStateToProps = state => {
  return {
    productsLoading: state.products.productsLoading,
    products: state.products.products,
    error: state.products.error
  };
};

export const mapDispatchToProps = dispatch => ({
  ProductsActions: bindActionCreators(ProductsActions, dispatch)
});

const index = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsContainer);

export default index;
