import React from 'react';
import { ProductsContainer } from '../ProductsContainer';

const props = {
  ProductsActions: {
    loadProducts: jest.fn()
  },
  productsLoading: true,
  products: [],
  error: false
};

describe('ProductsContainer container', () => {
  it('should render a loader when products are loading', () => {
    const wrapper = global.shallow(<ProductsContainer {...props} />);
    expect(wrapper.find('Loader').length).toBe(1);
  });
  it('should render products when products loaded', () => {
    const wrapper = global.shallow(
      <ProductsContainer
        {...props}
        productsLoading={false}
        products={[{ name: 'product1' }]}
      />
    );
    expect(wrapper.find('Loader').length).toBe(0);
    expect(wrapper.find('Products').length).toBe(1);
  });
  it('should redirect when error', () => {
    const wrapper = global.shallow(
      <ProductsContainer {...props} error={true} />
    );
    expect(wrapper.find('Loader').length).toBe(0);
    expect(wrapper.find('Products').length).toBe(0);
    expect(wrapper.find('Redirect').length).toBe(1);
    expect(wrapper.find('Redirect').props().to).toEqual('/ErrorPage');
  });
});
