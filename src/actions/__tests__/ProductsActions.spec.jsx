import * as actions from '../ProductsActions';
import * as types from '../actionTypes';

describe('ProductsActions', () => {
  it('should create an action to loadingProducts', () => {
    const expectedAction = {
      type: types.LOADING_PRODUCTS
    };
    expect(actions.loadingProducts()).toEqual(expectedAction);
  });

  it('should create an action to loadedProducts', () => {
    const expectedAction = {
      type: types.LOADED_PRODUCTS,
      payload: { products: [{ name: 'product1' }] }
    };
    expect(actions.loadedProducts([{ name: 'product1' }])).toEqual(
      expectedAction
    );
  });
  it('should create an action to errorProducts', () => {
    const expectedAction = {
      type: types.ERROR_PRODUCTS
    };
    expect(actions.errorProducts()).toEqual(expectedAction);
  });
});
