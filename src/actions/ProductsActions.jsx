import * as ProductsApi from '../api/ProductsApi';
import * as types from './actionTypes';

/*
 * action creators
 */

export const loadProducts = () => dispatch => {
  dispatch(loadingProducts());
  return ProductsApi.fetchProducts().then(
    response => {
      dispatch(loadedProducts(response));
    },
    () => {
      dispatch(errorProducts());
    }
  );
};

// not exported
export const loadingProducts = () => {
  return {
    type: types.LOADING_PRODUCTS
  };
};
export const loadedProducts = products => {
  return {
    type: types.LOADED_PRODUCTS,
    payload: {
      products
    }
  };
};

export const errorProducts = () => {
  return {
    type: types.ERROR_PRODUCTS
  };
};
