This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app and server in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
Open [http://localhost:9000](http://localhost:9000) to view the server.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn storybook`

runs Storybook on [http://localhost:9001](http://localhost:9001)

### `yarn test`

Launches the test runner in the interactive watch mode.
