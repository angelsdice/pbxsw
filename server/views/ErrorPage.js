import React from 'react';
import '../stylesheets/listingPage.scss';

const ErrorPage = () => <main>404 Error</main>;
export default ErrorPage;
