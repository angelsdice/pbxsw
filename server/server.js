// server/index.js
// 'use strict';
import path from 'path';
import express from 'express';
import handlebars from 'express-handlebars';
import api from './api';
const app = express();
const PORT = process.env.PORT || 9000;

api(app);

/**
 * View engine Handlebars
 * */
app.set('view engine', 'hbs');
app.engine(
  'hbs',
  handlebars({
    extname: '.hbs',
    partialsDir: path.resolve(__dirname, '../views/layout/partials')
  })
);
app.set('views', path.resolve(__dirname, '../views/layout/'));

// Serve static assets
app.use(express.static(path.resolve(__dirname, '..', 'build')));

// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});
