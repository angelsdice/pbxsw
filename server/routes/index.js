import React from 'react';
import { Router, Route, hashHistory } from 'react-router';

import ErrorBoundary from '../components/SharedComponents/ErrorBoundary/ErrorBoundary';
import HomePage from '../views/HomePage';
import ErrorPage from '../views/ErrorPage';

export default (
  <ErrorBoundary>
    <Router history={hashHistory}>
      <Route path="/" component={HomePage} />
      <Route path="*" component={ErrorPage} />
    </Router>
  </ErrorBoundary>
);
